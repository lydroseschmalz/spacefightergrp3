

#include "Level01.h"
#include "BioEnemyShip.h"
#include "GunnerEnemyShip.h"
#include "Blaster.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 21;
	const int BOSS = 20;

	double xPositions[COUNT + BOSS] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.3, 0.4, 0.5, 0.6, 0.7, //new ship position
		.5, .55, .45, .6, .4, .65, .35, .7, .3, .4, .5, .6, .7, .2, .3
	};
	
	double delays[COUNT + BOSS] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		3, .5, .5, .5, .5, //new ship delay
		3, .2, .2, .2, .2, .2, .2, .2, .2, 0, 0, 0, 0, 0, 0
	};

	float delay = 3.0; // start delay //was 3.0 //Now 3.0 again
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	for (int i = COUNT; i < (COUNT + BOSS); i++) { //new ship initialization
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		GunnerEnemyShip* pEnemy = new GunnerEnemyShip();
		
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

