#pragma once

#include "Weapon.h"

class SprayGun : public Weapon
{
public:
	SprayGun(const bool isActive) : Weapon(isActive)
	{
		m_projectilePath = -Vector2(0.4f, 1.0f);

		m_cooldown = 0;
		m_cooldownSeconds = .08;
	}

	virtual ~SprayGun() { }

	virtual char ToString() // Custom
	{
		return 'S';
	}

	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}

private:

	Vector2 m_projectilePath;

	float m_cooldown;
	float m_cooldownSeconds;
};