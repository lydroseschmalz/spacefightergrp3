#include "GunnerEnemyShip.h"


GunnerEnemyShip::GunnerEnemyShip() //NOT ACTUALLY GUNNER YET
{
	SetSpeed(75);
	SetMaxHitPoints(4);
	SetCollisionRadius(40);
}


void GunnerEnemyShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (GetHitPoints() < 3) 
		{
			SetSpeed(300);
		}

		if (!IsOnScreen()) Deactivate();

	}

	EnemyShip::Update(pGameTime);
}


void GunnerEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Pink, m_pTexture->GetCenter(), Vector2::Vector2(2, 2), Math::PI, 1);
	}
}