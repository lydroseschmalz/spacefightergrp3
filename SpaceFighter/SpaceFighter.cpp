
#include "KatanaEngine.h"
#include "SpaceFighter.h"
#include "MainMenuScreen.h"
#include "Projectile.h"
#include "windows.h"

using namespace KatanaEngine;

SpaceFighter::SpaceFighter()
{
	SetScreenResolution(1600, 900);
	SetFullScreen(false);

	InitializeScreenManager();

	SetResourceDirectory("..\\SpaceFighter\\Content\\");

	// Font for displaying the frame rate
	Font::SetLoadSize(18, true);
	Font *pFont = GetResourceManager()->Load<Font>("Fonts\\Arialbd.ttf", false);
	SetFrameCounterFont(pFont);

	// Playing song
	ALLEGRO_SAMPLE *song = al_load_sample("..\\SpaceFighter\\Content\\OffLimits.wav");

	ALLEGRO_SAMPLE_INSTANCE *songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());

	al_play_sample_instance(songInstance);


	//al_destroy_sample(song);
	//al_destroy_sample_instance(songInstance);
}


void SpaceFighter::Draw(SpriteBatch *pSpriteBatch)
{
	Game::Draw(pSpriteBatch);

	DisplayFrameRate();
}

void SpaceFighter::LoadContent(ResourceManager *pResourceManager) 
{
	// Load static resources
	Projectile::SetTexture(pResourceManager->Load<Texture>("Textures\\Bullet.png"));

	GetScreenManager()->AddScreen(new MainMenuScreen());

	ResetGameTime();
}